﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Eat : MonoBehaviour
{
    private Rigidbody2D rb2d;
    public int count;
    public Text countText;
    public float Increase;
    public Movement decreaseSpeed;
    public Eat2 countEnemy2;

    private int health;
    public Text healthBar;
    public bool LMActive;

    void Start()
    {
        count = 0;
        health = 3;
        SetCountText();
        SetHealthText();
    }

    void Update()
    {
        if (count == 280)
        {
            SceneManager.LoadScene("GameOver");
        }
        if(count < countEnemy2.count2 )
        {
            LMActive = true;
            Debug.Log("Kurang");
        }
        if(count >= countEnemy2.count2)
        {
            LMActive = false;
        }
        if (decreaseSpeed.speed < 5)
        {
            decreaseSpeed.speed = 5;
        }
    }



    void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject.CompareTag("Collectible"))
        {
            Debug.Log("Hit");
            other.gameObject.SetActive(false);
            count += 1;
            if (count % 5 == 0 && count != 0)
            {
                decreaseSpeed.speed += 0.5f;
                Debug.Log("-0.5");
            }
           
            SetCountText();
        }

        if (other.gameObject.CompareTag("Upgrade"))
        {

            Debug.Log("Upgrade");
            count += 5;
            decreaseSpeed.speed += 1f;
            transform.localScale += new Vector3(Increase, Increase);
            other.gameObject.SetActive(false);
            SetCountText();
        }

        if (other.gameObject.CompareTag("Bomb"))
        {
            Debug.Log("Bomb");
            count -= 5;
            decreaseSpeed.speed -= 1f;
            //transform.localScale -= 1.5f * new Vector3(Increase, Increase);
            other.gameObject.SetActive(false);
            SetCountText();
            SetHealthText();
        }

        if (other.gameObject.CompareTag("Wall"))
        {
            Debug.Log("Wall");
            count = 0;
            decreaseSpeed.speed = 30f;  
            health -= 1;
            SetHealthText();
            SetCountText();
        }
        if (other.gameObject.CompareTag("LandMine2"))
        {
            Debug.Log("LandMine2");
            count -= 10;
            countEnemy2.count2 += 20;
            decreaseSpeed.speed -= 1f;
            //health -= 1;
            other.gameObject.SetActive(false);
            countEnemy2.Setcount2Text();
            SetCountText();
            SetHealthText();
        }
    }
    void SetHealthText()
    {
        if (count < 0)
        {
            count = 0;
            health -= 1;
            SetCountText();
            SetHealthText();
        }
        if (health == 0)
        {
            SceneManager.LoadScene("GameOver2");
        }
        healthBar.text = "Health : " + health.ToString();
    }
    public void SetCountText()
    {
        countText.text = "Player 1 Score : " + count.ToString();
    }
}

