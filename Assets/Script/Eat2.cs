﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class Eat2 : MonoBehaviour
{
    private Rigidbody2D rb2d;
    public int count2;
    public Text count2Text;
    public float Increase2;
    public Movement2 decreaseSpeed2;
    public Eat countEnemy1;

    private int health2;
    public Text healthBar2;
    public bool LMActive2;

    void Start()
    {
        count2 = 0;
        Setcount2Text();
        health2 = 3;
        Sethealth2Text(); 
    }
    void Update()
    {
        if (count2 == 280)
        {
            SceneManager.LoadScene("GameOver2");
        }
        if (count2 < countEnemy1.count)
        {
            LMActive2 = true;
            Debug.Log("Kurang2");
        }
        if (count2 >= countEnemy1.count)
        {
            LMActive2 = false;
        }
        if (decreaseSpeed2.speed2 < 5)
        {
            decreaseSpeed2.speed2 = 5;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Collectible"))
        {
            Debug.Log("Hit 2");
            other.gameObject.SetActive(false);
            count2 += 1;
            if (count2 % 5 == 0 && count2 != 0)
            {
                decreaseSpeed2.speed2 += 0.5f;
                Debug.Log("-0.5");
            }
           
            Setcount2Text();
        }

        if (other.gameObject.CompareTag("Upgrade"))
        {
            Debug.Log("Upgrade");
            count2 += 5;
            decreaseSpeed2.speed2 += 1f;
            transform.localScale += new Vector3(Increase2, Increase2);
            other.gameObject.SetActive(false);
          
            Setcount2Text();
        }

        if (other.gameObject.CompareTag("Bomb"))
        {
            Debug.Log("Bomb");
            count2 -= 5;
            decreaseSpeed2.speed2 -= 1f;
            //transform.localScale -= 1.5f * new Vector3(Increase2, Increase2);
            other.gameObject.SetActive(false);
            Setcount2Text();
            Sethealth2Text();
            

        }
        if (other.gameObject.CompareTag("Wall"))
        {
            Debug.Log("Wall");
            count2 = 0;
            decreaseSpeed2.speed2 = 30f;
            health2 -= 1;
            Sethealth2Text();
            Setcount2Text();
        }
        if (other.gameObject.CompareTag("LandMine"))
        {
            Debug.Log("LandMine");
            count2 -= 10;
            decreaseSpeed2.speed2 -= 1f;
            countEnemy1.count += 20;
            //health2 -= 1;
            other.gameObject.SetActive(false);
            countEnemy1.SetCountText();
            Sethealth2Text();
            Setcount2Text();
        }
    }

    void Sethealth2Text()
    {
        if (count2 < 0)
        {
            count2 = 0;
            health2 -= 1;
            Setcount2Text();
            Sethealth2Text();
        }
        if (health2 == 0)
        {
            SceneManager.LoadScene("GameOver");
        }
        healthBar2.text = "Health : " + health2.ToString();
    }
    public void Setcount2Text()
    {
        count2Text.text = "Player 2 Score : " + count2.ToString();
    }
}
