﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LMSpawn : MonoBehaviour
{
    public GameObject LM;
    int numberOfLM = 1;
    public Eat activeLM;

    void Update()   
    {
        if (activeLM.LMActive == true)
        {
            if (Input.GetKeyDown(KeyCode.LeftControl) && numberOfLM >= 1)
            {
                Instantiate(LM, transform.position, Quaternion.identity);
                numberOfLM--;
                Invoke("AddLM", 1);
            }
        }
        if (activeLM.LMActive == false)
        {
            Debug.Log("INVALID, FALSE");
        }
    }

    public void AddLM()
    {
        numberOfLM++;
    }
  
}
