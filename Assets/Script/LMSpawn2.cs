﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LMSpawn2 : MonoBehaviour
{
    public GameObject LM2;
    int numberOfLM2 = 1;
    public Eat2 activeLM2;

    void Update()
    {
        if (activeLM2.LMActive2 == true)
        {
            if (Input.GetKeyDown(KeyCode.RightControl) && numberOfLM2 >= 1)
            {
                Instantiate(LM2, transform.position, Quaternion.identity);
                numberOfLM2--;
                Invoke("AddLM2", 1);
            }
        }
        if (activeLM2.LMActive2 == false)
        {
            Debug.Log("INVALID, FALSE");
        }
    }

    public void AddLM2()
    {
        numberOfLM2++;
    }

}
