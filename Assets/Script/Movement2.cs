﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Movement2 : MonoBehaviour
{
    public float speed2;
    private Rigidbody2D rb2d;
 
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }
    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal2");
        float moveVertical = Input.GetAxis("Vertical2");
        rb2d.velocity = new Vector2(moveHorizontal * speed2, moveVertical * speed2);
    }

    void update()
    {
        if(speed2 < 5)
        {
            speed2 = 5;
        }
    }
}

